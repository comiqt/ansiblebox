# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  config.vm.box = "geerlingguy/ubuntu1604"

  config.push.define "atlas" do |push|
    push.app = "comiq/ansiblebox"
  end

  config.vm.provider "virtualbox" do |v|
    v.name = "comiq-ansiblebox"
    v.memory = 2048
    v.cpus = 4
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "95"]
  end

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  # FIX FOR KNOWN ISSUE IN VAGRANT 1.8.1
  # Vagrant is incompatible with 2.x Ansible
  config.vm.provision :shell, inline: <<-SCRIPT
    GALAXY=/usr/local/bin/ansible-galaxy
    echo '#!/usr/bin/env bash
    /usr/bin/ansible-galaxy "$@"
    exit 0
    ' | sudo tee $GALAXY
    sudo chmod 0755 $GALAXY
  SCRIPT

  # FIX FOR KNOWN BUG IN VAGRANT 1.8.1
  # Vagrant tries to pass 'C:/'-prefix for playbook
  config.vm.provision :shell, inline: <<-SCRIPT
      if [ ! -d "/home/vagrant/C:/vagrant" ]; then
        mkdir /home/vagrant/C:
        ln -s /vagrant /home/vagrant/C:/vagrant
      fi
  SCRIPT

  # Provision using Ansible
  config.vm.provision "ansible_local" do |ansible|
      ansible.playbook      = "playbook.yml"
      ansible.verbose        = true
      ansible.install        = true
  end

end
